// Require the Bolt package (github.com/slackapi/bolt)
const { App } = require("@slack/bolt");
const { talkpageController } = require("./api");

require("dotenv").config();

const WikimediaStream = require("wikimedia-streams").default;

const stream = new WikimediaStream("recentchange");

const app = new App({
  token: process.env.SLACK_BOT_TOKEN,
  signingSecret: process.env.SLACK_SIGNING_SECRET,
});

let paramsValid = false;

let appState = { 
  url: undefined, 
  channel: undefined,
  channelId: undefined,
  userId: undefined
}

//TODO: 
// - add empty states
// - update message when a list item got deleted

const hasSlackChannel = (channelString) => {
 return channelString.startsWith("#")
}

const isValidUrl = urlString => {
  try { 
    return Boolean(new URL(urlString)); 
  }
  catch(e){ 
    return false; 
  }
}

const getChannel = (str) => {
  return str.substring(
    str.indexOf("#") + 1, 
    str.lastIndexOf(" "))
}

const getUrl = (str) => {
  return str.substring(str.indexOf("https://"),str.length)
}

const hasTalkPage = (str) => {
  return str.split(" ").length > 1 && str.includes("Talk") ;
}

app.command('/talkpage', async (params) => {
  const { ack, payload, logger } = params;
  
  appState["url"] = getUrl(payload.text)
  appState["channel"] = getChannel(payload.text)
  appState["userId"] = payload.user_id
  appState["channelId"] = payload.channel_id
  
  logger.info("Request made to bot" + payload.text)
  
  let errorMessage = '';
  //check for parameter to slash command that lists watched pages
  if (payload.text === "list"){
    //request user's watched talkpages from db
    //and list them in message
    talkpageController.getTalkpageBySlackUserId(appState["userId"]).then(value => {
      return value.data.map((page) => { 
        return {
          type: "section",
          text:  {
            type: "plain_text",
            text: `• On #${page.slack_channel_name} you are watching ${page.talk_url}\n`
          },
          accessory: {
            type: "button",
            text: {
              type: "plain_text",
              text: "Delete"
            },
            value: page.id,
            action_id: "deleteAction"
          }
        }
      })
    }).catch( (e) => {
      console.log(JSON.stringify(e));
    //render list watched pages with delete buttons or empty state
    }).then(listOfPages => listOfPages.length > 0 ?
       publishInteractiveMessage(payload.channel_name,"Currently Watched Pages", payload.user_id, listOfPages) 
       : publishInteractiveMessage(payload.channel_name,"Your are not watching any pages", payload.user_id, [])).catch( (e) => {
      console.log(JSON.stringify(e));
    })
  //validate parameter to slash command  
  } else if (!hasSlackChannel(payload.text)) {
    errorMessage = "Send a valid Slack channel after the command i.e. `#world`"
  } else if (!hasTalkPage(payload.text)) {
    errorMessage = "Send an url of a talk page that contains i.e. `Talk:`"
  } else if (!isValidUrl(getUrl(payload.text))) {
    errorMessage = "Send a valid talk page url after the channel name i.e. `https://en.wikipedia.org/wiki/Talk:Bot`"
  } else {
    paramsValid = true;
    errorMessage = "Your command is valid!"; 
    //store talkpage to watch in database
    talkpageController.createTalkpage({
      slack_channel_id: appState["channelId"],
      slack_channel_name: appState["channel"],
      slack_user_id: appState["userId"],
      talk_url: appState.url
    }).then(e => {
      console.log(e);
    }).catch( (e) => {
      console.dir(e);
    })
  }
  // Acknowledge the command request 
  // if it isn’t a valid command format, acknowledge with an error
  ack(errorMessage)

});

//use message_ts
async function updateMessage(channelName, ts) {
  try {
    const result = await app.client.chat.update({
      token: process.env.SLACK_BOT_TOKEN,
      channel: channelName,
      ts: ts
    });
  }
  catch (error) {
    console.error(error);
  }
}

async function publishMessage(channelName, text, user) {
  try {
    await app.client.chat.postEphemeral({
      token: process.env.SLACK_BOT_TOKEN,
      channel: channelName,
      user: user,
      text: text
    });
  }
  catch (error) {
    console.error(error);
  }
}

async function publishInteractiveMessage(channelName, text, user, buttonData) {
  try {
    await app.client.chat.postEphemeral({
      token: process.env.SLACK_BOT_TOKEN,
      channel: channelName,
      text: text,
      user: user,
      blocks: buttonData
    });
  }
  catch (error) {
    console.error(error);
  }
}


app.action('deleteAction', async ({ body, ack, say }) => {
  // Acknowledge the action
  await ack();
  talkpageController.deleteTalkPageById(body.actions[0].value).then(e => {
    console.log("BODY")
    console.log(body)
    updateMessage(body.channel.id,body.container.message_ts)
  }).catch( (e) => {
    console.dir(e);
  }) 
  console.log("Deleted", body)
  console.log(body)
  
  //Message(body.actions[0].channelName,body.actions[0].ts)
  say("You deleted talk page you were watching");
});

// start watching for changes once parameters are valid 
stream.on("recentchange", (data) => {

  if (paramsValid && data.type === "edit" && data.meta.uri === appState["url"]) {
    
    publishMessage(appState["channel"], `New Reply on talk page ${appState["url"]}`, appState["userId"]);
  } 
})
 
stream.on('error', (event) => {
  console.log(event);
});


(async () => {
  // Start your app
  const appPort = process.env.PORT || 3000
  try {
    await app.start(appPort);
  } catch (error) {
    console.error(error);
  }

  console.log(`⚡️ Bolt app is running on port ${appPort}!`);
})();

const db = require('../config/db.config.js');
const userController = require('./user.controller.js');

/**
 * Create a new Talkpage
 * @param {string} param.slack_channel_id Slack Channel ID
 * @param {string} param.slack_channel_name Slack Channel Name
 * @param {string} param.slack_user_id Slack User Id
 * @param {string} param.talk_url Wikimedia talk url
 * @returns {Promise<{ status: boolean, message: string, data: Talkpage}>} details of created talk page
 */
exports.createTalkpage = async ({
  slack_channel_id,
  slack_channel_name,
  slack_user_id,
  talk_url,
}) => {
  // Validate request
  if (!slack_channel_id || !slack_channel_name || !slack_user_id || !talk_url) {
    throw {
      status: false,
      message: 'Invalid Talkpage parameters!',
    };
  }

  try {
    // Check and Save User in the database if user does not exist
    await userController.createUser({ slack_user_id }).catch(( e ) => {
      if (e.message === 'duplicate key value violates unique constraint "users_pkey"') {
        return true;
      }
      throw e;
    });

    // Save Talkpage in the database
    await db.query(
      'INSERT INTO Talkpages (slack_user_id, slack_channel_name, slack_channel_id, talk_url) VALUES ($1, $2, $3, $4)',
      [slack_user_id, slack_channel_name, slack_channel_id, talk_url]
    );

    return {
      status: true,
      message: 'Talkpage was created successfully!',
      data: {
        slack_channel_id: slack_channel_id,
        slack_channel_name: slack_channel_name,
        slack_user_id: slack_user_id,
        talk_url: talk_url,
      },
    };
  } catch (error) {
    console.error('createTalkpage', error);
    throw {
      status: false,
      message:
        error.message || 'Some error occurred while creating the Talkpage.',
    };
  }
};

/**
 * Retrieve all User Talkpages from the database
 * @param {string} slackUserId Slack Channel ID
 * @returns {Promise<{ status: boolean, message: string, data: Array<Talkpage>>}} details of created talk page
 */
exports.getTalkpageBySlackUserId = async (slackUserId) => {
  if (!slackUserId) {
    throw new Error({
      status: false,
      message: 'slackUserId cannot be empty!',
    });
  }

  try {
    const { rows } = await db.query(
      `SELECT id, slack_channel_id, slack_channel_name, slack_user_id, talk_url FROM Talkpages WHERE slack_user_id = $1`,
      [slackUserId]
    );
    return {
      status: true,
      message: 'Talkpage was retrieved successfully!',
      data: rows,
    };
  } catch (error) {
    console.error('getTalkpageBySlackUserId', error);
    throw {
      status: false,
      message:
        error.message || 'Some error occurred while retrieving all Talkpages',
    };
  }
};

/**
 * Delete a Talkpage with the specified id
 * @param {string} id Item Row ID
 * @returns {Promise<{ status: boolean, message: string}}
 */
exports.deleteTalkPageById = async (id) => {
  try {
    await db.query('DELETE FROM Talkpages WHERE id = $1', [id]);
    return {
      status: true,
      message: 'Talkpage was deleted successfully!',
    };
  } catch (error) {
    console.error('deleteTalkPageById', error);
    throw {
      status: false,
      message: `Cannot delete Talkpage with id=${id}`,
    };
  }
};

const { Pool } = require('pg');
const dotenv = require('dotenv');
const databaseConfig = require('./database.json');

dotenv.config();

let pgConfig = {
  connectionString: process.env.DATABASE_CONNECTION_URL,
};

if (!pgConfig.connectionString) {
  pgConfig = {
    ...pgConfig,
    user: databaseConfig.dev.user,
    database: databaseConfig.dev.database,
    host: databaseConfig.dev.host,
    port: '5432',
    password: databaseConfig.dev.password,
  }; 
}


const pool = new Pool(pgConfig);

pool.on('error', (err) => {
  console.log('Unexpected error on idle client', err);
  process.exit(-1);
});

pool.on('connect', () => {
  console.log('Database connected!');
});

module.exports = {
  query: (text, params) => pool.query(text, params),
};

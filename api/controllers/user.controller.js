const db = require('../config/db.config.js');

/**
 * Create a new User
 * @param {string} params.slack_user_id Slack User ID
 * @param {string} params.slack_user_name Slack User Name
 * @returns {Promise<{ status: boolean, message: string, data: Array<User>>}} details of created user
 */
exports.createUser = async ({ slack_user_id, slack_user_name }) => {
  // Validate request
  if (!slack_user_id || slack_user_name) {
    throw new Error({
      status: false,
      message: 'slackUserId cannot be empty!',
    });
  }

  try {
    // Save User in the database
    await db.query(
      'INSERT INTO Users (slack_user_id, slack_user_name) VALUES ($1, $2)',
      [slack_user_id, slack_user_name]
    );

    return {
      status: true,
      message: 'User was created successfully!',
      data: {
        slack_user_id,
        slack_user_name,
      },
    };
  } catch (error) {
    // slience duplicate error
    if (error.message !== 'duplicate key value violates unique constraint "users_pkey"') {
      console.error('createUser', error);
    }
    throw {
      status: false,
      message: error.message || 'Some error occurred while creating the User.',
    };
  }
};

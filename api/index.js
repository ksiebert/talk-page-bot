const userController = require('./controllers/user.controller');
const talkpageController = require('./controllers/talkpage.controller');

module.exports = { userController, talkpageController };

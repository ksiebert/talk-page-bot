-- Table: Talkpages;

CREATE EXTENSION IF NOT EXISTS "uuid-ossp";

CREATE TABLE IF NOT EXISTS Talkpages (
	id uuid PRIMARY KEY NOT NULL DEFAULT uuid_generate_v4 (),
	slack_user_id VARCHAR(255) NOT NULL,
	slack_channel_name VARCHAR(255) NOT NULL,
	slack_channel_id VARCHAR(255) NOT NULL,
	talk_url VARCHAR(255) NOT NULL,
	FOREIGN KEY (slack_user_id) REFERENCES Users(slack_user_id)
)

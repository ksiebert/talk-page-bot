-- Table: Users;

CREATE TABLE IF NOT EXISTS Users (
	slack_user_id  VARCHAR(255) PRIMARY KEY NOT NULL,
	slack_user_name VARCHAR(255)
)